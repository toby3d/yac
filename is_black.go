package yac

import (
	"image/color"

	"github.com/lucasb-eyer/go-colorful"
)

// isBlack check what the current pixel is black.
func isBlack(c color.Color) bool {
	col, _ := colorful.MakeColor(c)
	black, _ := colorful.MakeColor(color.Black)
	return col == black
}
