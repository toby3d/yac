package yac

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

var testFiles []string

func colorName(i int) string {
	switch i {
	case 0:
		return "background"
	case 1:
		return "button"
	case 2:
		return "text"
	case 3:
		return "other"
	default:
		return ""
	}
}

func TestMain(m *testing.M) {
	filepath.Walk(
		filepath.Join(os.Getenv("GOPATH"), "src", "gitlab.com", "toby3d", "yac", "test"),
		func(path string, info os.FileInfo, err error) error {
			if err != nil || info.IsDir() {
				return err
			}

			testFiles = append(testFiles, path)
			return nil
		})

	os.Exit(m.Run())
}

func TestOpen(t *testing.T) {
	for _, testFile := range testFiles {
		_, filename := filepath.Split(testFile)
		t.Run(filename, func(t *testing.T) {
			img, err := Open(testFile)
			assert.NoError(t, err)
			assert.NotEmpty(t, img)
		})
	}
}

func TestAnalyzeFile(t *testing.T) {
	for _, testFile := range testFiles {
		_, filename := filepath.Split(testFile)
		t.Run(filename, func(t *testing.T) {
			colors, err := Analyze(testFile)
			assert.NoError(t, err)

			for j := range colors {
				r, g, b, _ := colors[j].RGBA()
				t.Logf("%s: rgb(%d, %d, %d)", colorName(j), r/256, g/256, b/256)
			}
		})
	}
}
