package yac

import (
	"image/color"
	"sort"

	"github.com/lucasb-eyer/go-colorful"
)

// Sort is a helper method which sort color.Color array of key colors by
// lightness. Returned color.Color array with sorted key colors: from the
// lightest to the darkest colors, if the current array is a light theme and
// vice versa, if it's dark.
func Sort(colors ...color.Color) []color.Color {
	if len(colors) == 0 {
		return nil
	}

	var light, dark int
	for i := range colors {
		c, ok := colorful.MakeColor(colors[i])
		if !ok {
			continue
		}

		_, _, l := c.Hsl()
		if l >= 0.5 {
			light++
			continue
		}

		dark++
	}

	sort.SliceStable(colors, func(i, j int) bool {
		ci, _ := colorful.MakeColor(colors[i])
		cj, _ := colorful.MakeColor(colors[j])
		_, _, li := ci.Hsl()
		_, _, lj := cj.Hsl()
		if light >= dark {
			return li > lj
		}

		return li < lj
	})

	return colors
}
