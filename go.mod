module gitlab.com/toby3d/yac

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/lucasb-eyer/go-colorful v0.0.0-20181028223441-12d3b2882a08
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
)
