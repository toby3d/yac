package yac

import (
	"image/color"

	"github.com/lucasb-eyer/go-colorful"
)

// IsLightScheme helper check first color.Color array element of sorted key
// colors on lightness. Returned true if current array contains colors for light
// scheme.
func IsLightScheme(colors ...color.Color) bool {
	if len(colors) == 0 {
		return false
	}

	c, ok := colorful.MakeColor(colors[0])
	if !ok {
		return false
	}

	_, _, L := c.Hsl()
	return L >= 0.5
}
