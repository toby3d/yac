package yac

import (
	"image/color"

	colorful "github.com/lucasb-eyer/go-colorful"
)

// isWhite helper check what the current pixel is white.
func isWhite(c color.Color) bool {
	col, _ := colorful.MakeColor(c)
	white, _ := colorful.MakeColor(color.White)
	return col == white
}
