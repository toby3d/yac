package yac

import (
	"image/color"

	"github.com/lucasb-eyer/go-colorful"
)

// IsDarkScheme helper check first color.Color array element of sorted key
// colors on lightness. Returned true if current array contains colors for dark
// scheme.
func IsDarkScheme(colors ...color.Color) bool {
	if len(colors) == 0 {
		return false
	}

	c, ok := colorful.MakeColor(colors[0])
	if !ok {
		return false
	}

	_, _, l := c.Hcl()
	return l < 0.5
}
