package yac

import "image/color"

// isAlpha check what the current pixel is any-transparant.
// TODO: I'm not sure that I need to filter "any-transparent" pixels...
func isAlpha(col color.Color) bool {
	_, _, _, a := col.RGBA()
	return a != 65535
}
