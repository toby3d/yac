package yac

import "image/color"

// Prune helper check Colors array on duplicated items. Returned Colors array
// with unique color.Color items in unsorted order.
func Prune(colors ...color.Color) []color.Color {
	if len(colors) == 0 {
		return nil
	}

	sorted := make(map[color.Color]bool)

	// Create a map of all unique elements.
	for i := range colors {
		sorted[colors[i]] = true
	}

	// Place all keys from the map into a slice.
	var unique []color.Color
	for key := range sorted {
		unique = append(unique, key)
	}

	return unique
}
