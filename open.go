package yac

import (
	"bytes"
	"errors"
	"image"
	_ "image/jpeg" // For JPEG data
	_ "image/png"  // For PNG data
	"io"
	"os"
)

// ErrUnsupportedType is returned in case if raw input data of image is
// not supported for decoding by Open method.
var ErrUnsupportedType = errors.New("use string, byte array or io.Reader only")

// Open method open a input data and try decode that as image.Image.
func Open(src interface{}) (img image.Image, err error) {
	switch src := src.(type) {
	case string: // File path
		file, err := os.Open(src)
		if err != nil {
			return nil, err
		}
		defer file.Close()

		img, _, err = image.Decode(file)
	case []byte: // Raw bytes
		img, _, err = image.Decode(bytes.NewReader(src))
	case io.Reader: // Reader
		img, _, err = image.Decode(src)
	case image.Image: // Already decoded image
		return src, nil
	default: // Unsupported
		err = ErrUnsupportedType
	}
	return
}
